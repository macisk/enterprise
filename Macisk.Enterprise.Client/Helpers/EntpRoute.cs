﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Macisk.Enterprise.Client.Helpers
{

    public static class EntpRouteConfig {


        public static string NoEnterpriseEndpoint
        {
            get { return "/Account/NoEnterprise"; }
        }
        /// <summary>
        /// Authentication Endpoint
        /// </summary>
        public static string NoneSSLAuthorizationServer
        {
            get { return "https://micchang.sce.pccu.edu.tw:44320"; }

        }
        /// <summary>
        /// Authentication Endpoint
        /// </summary>
        public static string NoneSSLAuthorizationServerPotocal
        {
            get { return NoneSSLAuthorizationServer.Split(new string[] { "://" }, StringSplitOptions.RemoveEmptyEntries)[0]; }

        }

        /// <summary>
        /// Authentication Endpoint
        /// </summary>
        public static string NoneSSLAuthorizationServerHost
        {
            get { return NoneSSLAuthorizationServer.Split(new string[] { "//" }, StringSplitOptions.RemoveEmptyEntries)[1]; }

        }

        /// <summary>
        /// Authentication Endpoint
        /// </summary>
        public static string AuthorizationServerHost
        {
            get { return AuthorizationServer.Split(new string[] { "//" }, StringSplitOptions.RemoveEmptyEntries)[1]; }

        }
        public static string AuthorizationServer { get { return "https://micchang.sce.pccu.edu.tw:44320"; } }


        /// <summary>
        /// Authentication Endpoint
        /// </summary>
        public static string AuthorizationServerPort
        {
            get
            {
                try
                {

                    return AuthorizationServer.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries)[1];
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
            }
        }

        public static string GetDomainByRouteSetting()
        {
            return string.Format("{0}", "localhost");
        }
        /// <summary>
        /// 取得企業路徑格式
        /// </summary>
        /// <returns></returns>
        public static string GetPathByRouteSetting()
        {
            if (RouteClientFunc == "Domain" || RouteClientFunc == "Fixed")
                return "{controller}/{action}";
            return "{entp}/{controller}/{action}";
        }

        public static string RouteServerFunc { get { return "Area"; } }
        public static string RouteClientFunc { get { return "Area"; } }
        public static string DefEntp { get { return "Macisk"; } }
        
        public static string ActiveEnterpriseEndpoint { get { return "/Account/IsActiveEnterprise"; } }

        public static string RouteDomain { get { return string.Format("{{entp}}.{0}", "localhost"); } }

        public static string RouteDefaultEntp { get { return "Macisk"; } }
    }


    public class EntpRoute : Route
    {
        public static EntpRoute StateRoute { get; set; }

        public string DefEntp
        {
            get
            {
                var Route = ((System.Web.HttpRequestWrapper)(new HttpContextWrapper(HttpContext.Current)).Request).RequestContext.RouteData;
                object Entp = string.Empty;
                object RouteEntp = string.Empty;
                Route.Values.TryGetValue("entp", out RouteEntp);
                if (!string.IsNullOrEmpty((string)RouteEntp)) Entp = RouteEntp;
                return (string)Entp;
            }
        }

        public EntpRuleBase EntpRule { get; set; }

        private Regex domainRegex;

        private Regex pathRegex;

        public string Domain { get; set; }

        public object Default { get; set; }


        #region 建構(Fixed)

        public EntpRoute(string domain, string url, RouteValueDictionary defaults, IRouteChecker RouteCheck = null, IUrlRule ServerRule = null, IUrlRule ClientRule = null)
            : base(url, defaults, new MvcRouteHandler())
        {
            this.EntpRule = GetEntpDomainRule(defaults, RouteCheck, ServerRule, ClientRule);
            this.Domain = domain;
            StateRoute = this;
        }

        public EntpRoute(string domain, string url, RouteValueDictionary defaults, IRouteHandler routeHandler, IRouteChecker RouteCheck = null, IUrlRule ServerRule = null, IUrlRule ClientRule = null)
            : base(url, defaults, routeHandler)
        {
            this.EntpRule = GetEntpDomainRule(defaults, RouteCheck, ServerRule, ClientRule);
            this.Domain = domain;
            StateRoute = this;
        }

        public EntpRoute(string domain, string url, object defaults, IRouteChecker RouteCheck = null, IUrlRule ServerRule = null, IUrlRule ClientRule = null)
            : base(url, new RouteValueDictionary(defaults), new MvcRouteHandler())
        {
            this.EntpRule = GetEntpDomainRule(defaults, RouteCheck, ServerRule, ClientRule);
            this.Domain = domain;
            StateRoute = this;
        }

        public EntpRoute(string domain, string url, object defaults, IRouteHandler routeHandler, IRouteChecker RouteCheck = null, IUrlRule ServerRule = null, IUrlRule ClientRule = null)
            : base(url, new RouteValueDictionary(defaults), routeHandler)
        {
            this.EntpRule = GetEntpDomainRule(defaults, RouteCheck, ServerRule, ClientRule);
            this.Domain = domain;
            StateRoute = this;
        }

        #endregion

        public override RouteData GetRouteData(HttpContextBase httpContext)
        {

            domainRegex = EntpRule.CreateDomainRegex(Domain);
            pathRegex = EntpRule.CreatePathRegex(Url);

            string requestDomain = httpContext.Request.Headers["host"];
            if (!string.IsNullOrEmpty(requestDomain))
            {
                if (requestDomain.IndexOf(":") > 0)
                {
                    requestDomain = requestDomain.Substring(0, requestDomain.IndexOf(":"));
                }
            }
            else
            {
                requestDomain = httpContext.Request.Url.Host;
            }

            string requestPath = httpContext.Request.AppRelativeCurrentExecutionFilePath.Substring(2) +
                                 httpContext.Request.PathInfo;

            Match domainMatch = domainRegex.Match(requestDomain);
            Match pathMatch = pathRegex.Match(requestPath);


            RouteData Result = null;

            if (EntpRule.IsMyRight(domainMatch, pathMatch))
            {
                Result = new RouteData(this, RouteHandler);

                Result = EntpRule.GetRouteData(httpContext, Result, Default, domainRegex, domainMatch, pathRegex, pathMatch);

                return Result;

                //if (EntpRule.ValidRoute(httpContext, Result, Default)) 
            }
            return Result;
        }

        public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
        {
            var Default = base.GetVirtualPath(requestContext, RemoveDomainTokens(values));

            return Default;
        }

        private RouteValueDictionary RemoveDomainTokens(RouteValueDictionary values)
        {
            var tokenRegex =
                new Regex(
                    @"({[a-zA-Z0-9_\-]*})*\.?\/?({[a-zA-Z0-9_\-]*})*\.?\/?({[a-zA-Z0-9_\-]*})*\.?\/?({[a-zA-Z0-9_\-]*})*\.?\/?({[a-zA-Z0-9_\-]*})*\.?\/?({[a-zA-Z0-9_\-]*})*\.?\/?({[a-zA-Z0-9_\-]*})*\.?\/?({[a-zA-Z0-9_\-]*})*\.?\/?({[a-zA-Z0-9_\-]*})*\.?\/?({[a-zA-Z0-9_\-]*})*\.?\/?({[a-zA-Z0-9_\-]*})*\.?\/?({[a-zA-Z0-9_\-]*})*\.?\/?");
            Match tokenMatch = tokenRegex.Match(Domain);
            for (int i = 0; i < tokenMatch.Groups.Count; i++)
            {
                Group group = tokenMatch.Groups[i];
                if (group.Success)
                {
                    string key = group.Value.Replace("{", "").Replace("}", "");
                    if (values.ContainsKey(key))
                        values.Remove(key);
                }
            }
            return values;
        }

        public static EntpRuleBase GetEntpDomainRule(object Defaults, IRouteChecker RouteCheck, IUrlRule ServerRule, IUrlRule ClientRule)
        {
            if (RouteCheck == null)
                RouteCheck = new ClientRouteChecker();

            if (ServerRule == null)
                ServerRule = GetUrlRule(EntpRouteConfig.RouteServerFunc);

            if (ClientRule == null)
                ClientRule = GetUrlRule(EntpRouteConfig.RouteClientFunc);

            EntpRuleBase Rule = null;
            switch (EntpRouteConfig.RouteClientFunc)
            {
                case "Domain":
                    Rule = new EntpDomainRule(RouteCheck, ServerRule, ClientRule, Defaults);
                    break;
                case "Area":
                    Rule = new EntpAreaRule(RouteCheck, ServerRule, ClientRule, Defaults);
                    break;
                case "Fixed":
                    Rule = new EntpFixedRule(RouteCheck, ServerRule, ClientRule, Defaults);
                    break;
                case "ApiDomain":
                    Rule = new ApiEntpDomainRule(RouteCheck, ServerRule, ClientRule, Defaults);
                    break;
                default:
                    Rule = new EntpDomainRule(RouteCheck, ServerRule, ClientRule, Defaults);
                    break;
            }
            return Rule;
        }

        public static IUrlRule GetUrlRule(string RuleCode)
        {
            switch (RuleCode)
            {
                case "Domain":
                    return new DomainUrlRule();
                case "Area":
                    return new AreaUrlRule();
                case "ApiDomain":
                case "Fixed":
                    return new FixedUrlRule();
                default:
                    return new FixedUrlRule();
            }

        }
    }

    public class EntpRuleBase
    {
        public RouteData RouteData
        {
            get
            {
                return ((System.Web.HttpRequestWrapper)(new HttpContextWrapper(HttpContext.Current)).Request).RequestContext.RouteData;
            }
        }
        public EntpRoute Route
        {
            get
            {
                return ((EntpRoute)RouteData.Route);
            }
        }
        public JObject Default { get; set; }
        IRouteChecker RouteChecker { get; set; }
        IUrlRule ServerRule { get; set; }
        IUrlRule ClientRule { get; set; }
        public EntpRuleBase(IRouteChecker RouteChecker, IUrlRule ServerRule = null, IUrlRule ClientRule = null)
        {
            this.RouteChecker = RouteChecker;
            this.ServerRule = ServerRule ?? new AreaUrlRule();
            this.ClientRule = ClientRule ?? new DomainUrlRule();
        }
        public virtual Regex CreateDomainRegex(string source) { throw new Exception(""); }
        public virtual Regex CreatePathRegex(string source) { throw new Exception(""); }
        public virtual RouteData GetRouteData(HttpContextBase httpContext, RouteData RouteData, object Default, Regex DomainRegex, Match DomainMatch, Regex PathRegex, Match PathMatch) { throw new Exception(""); }

        public virtual bool IsMyRight(Match DomainMatch, Match PathMatch)
        {
            return false;
        }

        #region For Valid 
        //public virtual string ValidRoute()
        //{
        //    return ValidRoute(HttpContext.Current, RouteData, Default);
        //}

        //public virtual string ValidRoute(HttpContext httpContext, RouteData RouteData, object Default)
        //{
        //    string
        //        RouteSubDomain = ((string)RouteData.Values["entp"] ?? string.Empty).ToLower(),
        //        RouteController = ((string)RouteData.Values["controller"] ?? string.Empty).ToLower(),
        //        RouteAction = ((string)RouteData.Values["action"] ?? string.Empty).ToLower(),
        //        Code = httpContext.Request.QueryString["code"],
        //        State = httpContext.Request.QueryString["state"];

        //    var RedirectUrl = string.Empty;

        //    if (!string.IsNullOrEmpty(RedirectUrl = RedirectForErrorDomain(RouteSubDomain)))
        //        return RedirectUrl;

        //    if (!string.IsNullOrEmpty(RedirectUrl = RedirectForNoAction(RouteSubDomain, RouteController, RouteAction)))
        //        return RedirectUrl;

        //    if (!string.IsNullOrEmpty(RedirectUrl = RedirectForNoDomain(RouteSubDomain, RouteController, RouteAction)))
        //        return RedirectUrl;

        //    return string.Empty;
        //}


        protected string RedirectForErrorDomain(string Domain)
        {
            try
            {
                var IsActiveEntps = RouteChecker.IsActiveEnterprise(Domain);
                if (IsActiveEntps) return null;
                return string.Format("{0}{1}", EntpRouteConfig.AuthorizationServer, EntpRouteConfig.NoEnterpriseEndpoint);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        protected string RedirectForNoAction(string Domain, string Controller, string Action)
        {
            if (string.IsNullOrEmpty(Controller) || string.IsNullOrEmpty(Action))
            {
                var EntpPath = string.Format("/{0}/{1}/{2}", Domain, "record", "cardlog");
                return EntpPath;
            }
            return null;
        }

        //protected string RedirectForNoDomain(string Domain, string Controller, string Action)
        //{
        //    try
        //    {
        //        var Router = ((System.Web.HttpRequestWrapper)(new HttpContextWrapper(HttpContext.Current)).Request).RequestContext.RouteData;

        //        var hasSubDomain = QuboUserManager.User.Enterprises.Where(c => c.EntpDomain.ToLower() == Domain.ToLower()).Count() > 0;
        //        if (!hasSubDomain)
        //        {
        //            var EntpPath = string.Format("/{0}/{1}/{2}", QuboUserManager.CurrentEntp.ToLower(), Controller, Action);
        //            return EntpPath;
        //        }
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        #endregion

        #region UrlParse

        #region Client 

        public virtual string EntpEndpoint(string Arguments, string Path, string EntpDomain, string Host, string Potocal = "http")
        {
            var NodePath = string.Format("{0}{1}{2}{3}",
                BuildHost(EntpDomain, Host, Potocal),
                BuildPath(Path, EntpDomain),
                (string.IsNullOrEmpty(Arguments) ? string.Empty : "?"),
                Arguments
            );
            return NodePath;
        }


        public string EntpPath(string Path, string Enterprise = null)
        {
            return BuildPath(Path, Enterprise);
        }
        //TODO:BUG
        public string EntpAction(string Action, string Controller, string EntpDomain = null)
        {
            try
            {
                var Path = string.Format("/{0}/{1}", Controller, Action);
                return EntpPath(Path, EntpDomain);
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public virtual string BuildHost(string EntpDomain, string Host, string Potocal)
        {
            var NodePath = string.Format("{0}://{1}/{2}",
                Potocal,
                Host,
                EntpDomain
            );
            return NodePath;
        }

        public virtual string BuildPath(string Path, string EntpDomain)
        {
            return Path;
        }

        public virtual string ClientPath(string Path, string EntpDomain = null)
        {
            return ClientRule.ServerPath(Path, EntpDomain);
        }

        public virtual string ClientHost(string EntpDomain, string Host, string Potocal)
        {
            return ClientRule.ServerHost(EntpDomain, Host, Potocal);
        }


        public virtual string ClientEndpoint(string Arguments, string Path, string EntpDomain, string Host, string Potocal = "http")
        {
            var NodePath = string.Format("{0}{1}{2}{3}",
                ClientHost(EntpDomain, Host, Potocal),
                ClientPath(Path, EntpDomain),
                (string.IsNullOrEmpty(Arguments) ? string.Empty : "?"),
                Arguments
            );
            return NodePath;
        }
        #endregion

        #region Server 
        public virtual string ServerHost(string EntpDomain = null, bool IsUseSSL = true)
        {
            return ServerRule.ServerHost(EntpDomain ?? EntpRouteConfig.DefEntp, IsUseSSL);
        }

        public virtual string ServerPath(string Path, string EntpDomain = null)
        {
            return ServerRule.ServerPath(Path, EntpDomain ?? EntpRouteConfig.DefEntp);
        }

        public virtual string ServerAction(string Controller, string Action, string EntpDomain = null)
        {
            var Path = string.Format("/{0}/{1}", Controller, Action);
            return ServerRule.ServerPath(Path, EntpDomain ?? EntpRouteConfig.DefEntp);
        }
        #endregion

        #endregion

    }

    public class ClientRouteChecker : IRouteChecker
    {
        public bool IsActiveEnterprise(string Domain)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var Endpoint = string.Format("{0}{1}?Domain={2}", EntpRouteConfig.AuthorizationServer, EntpRouteConfig.ActiveEnterpriseEndpoint, Domain);
                    var content = client.GetStringAsync(Endpoint).GetAwaiter().GetResult();
                    var Result = JsonConvert.DeserializeObject<bool>(content);
                    return Result;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }

    public interface IRouteChecker
    {
        bool IsActiveEnterprise(string Domain);
    }

    public class EntpDomainRule : EntpRuleBase
    {
        public EntpDomainRule(IRouteChecker RouteChecker, IUrlRule ServerRule = null, IUrlRule ClientRule = null, object Default = null) : base(RouteChecker, ServerRule, ClientRule)
        {
            if (Default != null)
                this.Default = JObject.FromObject(Default);
        }

        public override bool IsMyRight(Match DomainMatch, Match PathMatch)
        {
            return DomainMatch.Success && PathMatch.Success;
        }

        public override Regex CreateDomainRegex(string source)
        {
            source = source.Replace("/", @"\/?");
            source = source.Replace(".", @"\.?");
            source = source.Replace("-", @"\-?");
            source = source.Replace("{", @"(?<");
            source = source.Replace("}", @">([a-zA-Z0-9_\-]*))");
            return new Regex("^" + source + "$");
        }

        public override Regex CreatePathRegex(string source)
        {
            // Perform replacements
            source = source.Replace("/", @"\/?");
            source = source.Replace(".", @"\.?");
            source = source.Replace("-", @"\-?");
            source = source.Replace("{", @"(?<");
            source = source.Replace("}", @">([a-zA-Z0-9_\-]*))");
            return new Regex("^" + source + "$");
        }

        public override RouteData GetRouteData(HttpContextBase httpContext, RouteData RouteData, object Default, Regex DomainRegex, Match DomainMatch, Regex PathRegex, Match PathMatch)
        {

            #region get Domain route data and  Path route data
            // Iterate matching domain groups
            for (int i = 1; i < DomainMatch.Groups.Count; i++)
            {
                Group group = DomainMatch.Groups[i];
                if (group.Success)
                {
                    string key = DomainRegex.GroupNameFromNumber(i);

                    if (!string.IsNullOrEmpty(key) && !char.IsNumber(key, 0))
                    {
                        if (!string.IsNullOrEmpty(group.Value))
                        {
                            RouteData.Values[key] = group.Value;
                        }
                    }
                }
            }

            // Iterate matching path groups
            for (int i = 1; i < PathMatch.Groups.Count; i++)
            {
                Group group = PathMatch.Groups[i];
                if (group.Success)
                {
                    string key = PathRegex.GroupNameFromNumber(i);

                    if (!string.IsNullOrEmpty(key) && !char.IsNumber(key, 0))
                    {
                        if (!string.IsNullOrEmpty(group.Value))
                        {
                            RouteData.Values[key] = group.Value;
                        }
                    }
                }
            }

            #endregion

            if (RouteData.Values["controller"] == null)
                RouteData.Values.Add("controller", "Record");

            if (RouteData.Values["action"] == null)
                RouteData.Values.Add("action", "CardLog");
            return RouteData;

        }

        public override string BuildPath(string Path, string Enterprise = null)
        {
            string SubDomain = (Enterprise ?? Route.DefEntp).ToLower();
            var Segments = Path.Split(new string[] { "~", "/" }, StringSplitOptions.RemoveEmptyEntries);
            return string.Format("/{0}", string.Join("/", Segments));
        }
        public override string BuildHost(string EntpDomain, string Host, string Potocal)
        {
            var NodePath = string.Format("{0}://{1}.{2}",
                Potocal,
                EntpDomain,
                Host
            );
            return NodePath;
        }

    }

    public class EntpAreaRule : EntpRuleBase
    {
        public EntpAreaRule(IRouteChecker RouteChecker, IUrlRule ServerRule = null, IUrlRule ClientRule = null, object Default = null) : base(RouteChecker, ServerRule, ClientRule)
        {
            if (Default != null)
                this.Default = JObject.FromObject(Default);
        }

        public override bool IsMyRight(Match DomainMatch, Match PathMatch)
        {
            return PathMatch.Success;
        }

        public override Regex CreateDomainRegex(string source)
        {
            source = source.Replace("/", @"\/?");
            source = source.Replace(".", @"\.?");
            source = source.Replace("-", @"\-?");
            source = source.Replace("{", @"(?<");
            source = source.Replace("}", @">([a-zA-Z0-9_\-]*))");
            return new Regex("^" + source + "$");
        }

        public override Regex CreatePathRegex(string source)
        {
            // Perform replacements
            source = source.Replace("/", @"\/?");
            source = source.Replace(".", @"\.?");
            source = source.Replace("-", @"\-?");
            source = source.Replace("{", @"(?<");
            source = source.Replace("}", @">([a-zA-Z0-9_\-]*))");
            return new Regex("^" + source + "$");
        }

        public override RouteData GetRouteData(HttpContextBase httpContext, RouteData RouteData, object Default, Regex DomainRegex, Match DomainMatch, Regex PathRegex, Match PathMatch)
        {

            #region get Domain route data and  Path route data
            // Iterate matching domain groups
            for (int i = 1; i < DomainMatch.Groups.Count; i++)
            {
                Group group = DomainMatch.Groups[i];
                if (group.Success)
                {
                    string key = DomainRegex.GroupNameFromNumber(i);

                    if (!string.IsNullOrEmpty(key) && !char.IsNumber(key, 0))
                    {
                        if (!string.IsNullOrEmpty(group.Value))
                        {
                            RouteData.Values[key] = group.Value;
                        }
                    }
                }
            }

            // Iterate matching path groups
            for (int i = 1; i < PathMatch.Groups.Count; i++)
            {
                Group group = PathMatch.Groups[i];
                if (group.Success)
                {
                    string key = PathRegex.GroupNameFromNumber(i);

                    if (!string.IsNullOrEmpty(key) && !char.IsNumber(key, 0))
                    {
                        if (!string.IsNullOrEmpty(group.Value))
                        {
                            RouteData.Values[key] = group.Value;
                        }
                    }
                }
            }

            #endregion

            if (RouteData.Values["controller"] == null)
                RouteData.Values.Add("controller", "Record");

            if (RouteData.Values["action"] == null)
                RouteData.Values.Add("action", "CardLog");
            return RouteData;

        }

        public override string BuildPath(string Path, string EntpDomain = null)
        {
            EntpDomain = (EntpDomain ?? Route.DefEntp).ToLower();
            var Segments = Path.Split(new string[] { "~", "/" }, StringSplitOptions.RemoveEmptyEntries);
            return string.Format("/{0}/{1}", EntpDomain, string.Join("/", Segments));
        }

        public override string BuildHost(string EntpDomain, string Host, string Potocal)
        {
            var NodePath = string.Format("{0}://{1}",
                Potocal,
                Host
            );
            return NodePath;
        }

        //public override string BuildRelativeUrl(string Action, string Controller, string Enterprise)
        //{
        //    var Route = ((System.Web.HttpRequestWrapper)(new HttpContextWrapper(HttpContext.Current)).Request).RequestContext.RouteData;
        //    //string SubDomain = ((string)Route.Values["entp"] ?? string.Empty).ToLower();
        //    var Url = HttpContext.Current.Request.Url;
        //    var EntpUrl = string.Format("/{0}/{1}/{2}/{3}",
        //                                          //Url.GetLeftPart(UriPartial.Authority),
        //                                          Enterprise,
        //                                          Controller,
        //                                          Action,
        //                                          Url.Query
        //                                          ).Replace("//", "/");
        //    return EntpUrl;
        //}


    }

    public class EntpFixedRule : EntpRuleBase
    {
        public EntpFixedRule(IRouteChecker RouteChecker, IUrlRule ServerRule = null, IUrlRule ClientRule = null, object Default = null) : base(RouteChecker, ServerRule, ClientRule)
        {
            if (Default != null)
                this.Default = JObject.FromObject(Default);
        }


        public override bool IsMyRight(Match DomainMatch, Match PathMatch)
        {
            return true;
        }

        public override Regex CreateDomainRegex(string source)
        {
            source = source.Replace("/", @"\/?");
            source = source.Replace(".", @"\.?");
            source = source.Replace("-", @"\-?");
            source = source.Replace("{", @"(?<");
            source = source.Replace("}", @">([a-zA-Z0-9_\-]*))");
            return new Regex("^" + source + "$");
        }

        public override Regex CreatePathRegex(string source)
        {
            // Perform replacements
            source = source.Replace("/", @"\/?");
            source = source.Replace(".", @"\.?");
            source = source.Replace("-", @"\-?");
            source = source.Replace("{", @"(?<");
            source = source.Replace("}", @">([a-zA-Z0-9_\-]*))");
            return new Regex("^" + source + "$");
        }

        public override RouteData GetRouteData(HttpContextBase httpContext, RouteData RouteData, object Default, Regex DomainRegex, Match DomainMatch, Regex PathRegex, Match PathMatch)
        {

            #region get Domain route data and  Path route data
            // Iterate matching domain groups
            for (int i = 1; i < DomainMatch.Groups.Count; i++)
            {
                Group group = DomainMatch.Groups[i];
                if (group.Success)
                {
                    string key = DomainRegex.GroupNameFromNumber(i);

                    if (!string.IsNullOrEmpty(key) && !char.IsNumber(key, 0))
                    {
                        if (!string.IsNullOrEmpty(group.Value))
                        {
                            RouteData.Values[key] = group.Value;
                        }
                    }
                }
            }

            // Iterate matching path groups
            for (int i = 1; i < PathMatch.Groups.Count; i++)
            {
                Group group = PathMatch.Groups[i];
                if (group.Success)
                {
                    string key = PathRegex.GroupNameFromNumber(i);

                    if (!string.IsNullOrEmpty(key) && !char.IsNumber(key, 0))
                    {
                        if (!string.IsNullOrEmpty(group.Value))
                        {
                            RouteData.Values[key] = group.Value;
                        }
                    }
                }
            }

            #endregion

            RouteData.Values["entp"] = this.Default.GetValue("entp").ToString();

            if (RouteData.Values["controller"] == null)
                RouteData.Values.Add("controller", "Record");

            if (RouteData.Values["action"] == null)
                RouteData.Values.Add("action", "CardLog");
            return RouteData;

        }
        public override string EntpEndpoint(string Arguments, string Endpoint, string EntpDomain, string Host, string Potocal)
        {
            var NodePath = string.Format("{0}://{1}/{2}{3}{4}",
                Potocal,
                Host,
                Endpoint,
                (string.IsNullOrEmpty(Arguments) ? string.Empty : "?"),
                Arguments
            );
            return NodePath;
        }

    }

    public class ApiEntpDomainRule : EntpDomainRule
    {
        public ApiEntpDomainRule(IRouteChecker RouteChecker, IUrlRule ServerRule = null, IUrlRule ClientRule = null, object Default = null) : base(RouteChecker, ServerRule, ClientRule)
        {
            if (Default != null)
                this.Default = JObject.FromObject(Default);
        }

        public override bool IsMyRight(Match DomainMatch, Match PathMatch)
        {
            return DomainMatch.Success;
        }
    }

    public interface IUrlRule
    {
        string ServerHost(string EntpDomain, bool IsUseSSL);
        string ServerPath(string Path, string EntpDomain);
        string ServerHost(string EntpDomain, string Host, string Potocal);

    }

    public class DomainUrlRule : IUrlRule
    {
        public string ServerHost(string EntpDomain, bool IsUseSSL)
        {
            var Host = (IsUseSSL) ? EntpRouteConfig.AuthorizationServerHost : EntpRouteConfig.NoneSSLAuthorizationServerHost;
            var Potocal = (IsUseSSL) ? "https" : "http";

            var NodePath = string.Format("{0}://{1}.{2}",
                Potocal,
                EntpDomain,
                Host
            );
            return NodePath;
        }

        public string ServerPath(string Path, string EntpDomain)
        {
            string SubDomain = EntpDomain.ToLower();
            var Segments = Path.Split(new string[] { "~", "/" }, StringSplitOptions.RemoveEmptyEntries);
            return string.Format("/{0}", string.Join("/", Segments));
        }

        public string ServerHost(string EntpDomain, string Host, string Potocal)
        {
            var NodePath = string.Format("{0}://{1}.{2}",
                Potocal,
                EntpDomain,
                Host
            );
            return NodePath;
        }

    }
    public class AreaUrlRule : IUrlRule
    {
        public string ServerHost(string EntpDomain, bool IsUseSSL = true)
        {
            var Host = (IsUseSSL) ? EntpRouteConfig.AuthorizationServerHost : EntpRouteConfig.NoneSSLAuthorizationServerHost;
            var Potocal = (IsUseSSL) ? "https" : "http";
            var NodePath = string.Format("{0}://{1}",
                Potocal,
                Host
            );
            return NodePath;
        }

        public string ServerPath(string Path, string EntpDomain)
        {
            EntpDomain = EntpDomain.ToLower();
            var Segments = Path.Split(new string[] { "~", "/" }, StringSplitOptions.RemoveEmptyEntries);
            return string.Format("/{0}/{1}", EntpDomain, string.Join("/", Segments)).Replace("//", "/");
        }

        public string ServerHost(string EntpDomain, string Host, string Potocal)
        {
            var NodePath = string.Format("{0}://{1}/{2}",
                Potocal,
                Host,
                EntpDomain
            );
            return NodePath;
        }
    }

    public class FixedUrlRule : IUrlRule
    {
        public string ServerHost(string EntpDomain, bool IsUseSSL = true)
        {
            var Host = (IsUseSSL) ? EntpRouteConfig.AuthorizationServerHost : EntpRouteConfig.NoneSSLAuthorizationServerHost;
            var Potocal = (IsUseSSL) ? "https" : "http";
            var NodePath = string.Format("{0}://{1}",
                Potocal,
                Host
            );
            return NodePath;
        }

        public string ServerPath(string Path, string EntpDomain)
        {
            EntpDomain = EntpDomain.ToLower();
            var Segments = Path.Split(new string[] { "~", "/" }, StringSplitOptions.RemoveEmptyEntries);
            return string.Format("/{0}", string.Join("/", Segments)).Replace("//", "/");
        }

        public string ServerHost(string EntpDomain, string Host, string Potocal)
        {
            var NodePath = string.Format("{0}://{1}",
                Potocal,
                Host
            );
            return NodePath;
        }
    }

}
