﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Macisk.Enterprise.Portal.Controllers
{
    public class RewriteController : Controller
    {
        // GET: Rewrite
        public ActionResult Info()
        {
            ViewBag.OriUrl = Request.ServerVariables["HTTP_X_ORIGINAL_URL"];
            ViewBag.RewriteUrl = Request.ServerVariables["SCRIPT_NAME"] + "?" + Request.ServerVariables["QUERY_STRING"];
            return View();
        }
    }
}